
public interface Serializable
{
	public abstract void load(String path, String filename);
	
	public abstract void save(String path, String filename);
}
