import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="medialist")
public class MediaList implements Iterable<Medium>
{
	@XmlElementWrapper(name = "List")
	ArrayList<Medium> media = new ArrayList<Medium>();

	public void add(Medium m)
	{
		media.add(m);
	}

	@Override
	public Iterator<Medium> iterator()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public void remove(Medium m)
	{
		media.remove(m);
	}
	
	
}
