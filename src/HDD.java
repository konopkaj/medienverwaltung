import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "hdd")
public class HDD extends Digital
{
	private Constants.HDD_SIZE size;

	/**
	 * @return the size
	 */
	public Constants.HDD_SIZE getSize()
	{
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(Constants.HDD_SIZE size)
	{
		this.size = size;
	}
}
