import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "journal")
public class Journal extends Analog
{
	private int month;
	private String ISSN;
	/**
	 * @return the month
	 */
	public int getMonth()
	{
		return month;
	}
	/**
	 * @return the iSSN
	 */
	public String getISSN()
	{
		return ISSN;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(int month)
	{
		this.month = month;
	}
	/**
	 * @param iSSN the iSSN to set
	 */
	public void setISSN(String iSSN)
	{
		ISSN = iSSN;
	}
}
