import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "digital")
public class Digital extends Medium
{
	private String version;
	private String dataVolume;
	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}
	/**
	 * @return the dataVolume
	 */
	public String getDataVolume()
	{
		return dataVolume;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version)
	{
		this.version = version;
	}
	/**
	 * @param dataVolume the dataVolume to set
	 */
	public void setDataVolume(String dataVolume)
	{
		this.dataVolume = dataVolume;
	}
	
	
}
