import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "disk")
public class CD extends Digital
{
	private boolean copyProtected;

	/**
	 * @return the copyProtected
	 */
	public boolean isCopyProtected()
	{
		return copyProtected;
	}

	/**
	 * @param copyProtected the copyProtected to set
	 */
	public void setCopyProtected(boolean copyProtected)
	{
		this.copyProtected = copyProtected;
	}
	
}
