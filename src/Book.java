import javax.xml.bind.annotation.*;

@XmlRootElement(name = "book")
public class Book extends Analog
{
	private String ISBN;

	/**
	 * @return the iSBN
	 */
	public String getISBN()
	{
		return ISBN;
	}

	/**
	 * @param iSBN the iSBN to set
	 */
	public void setISBN(String iSBN)
	{
		ISBN = iSBN;
	}
}
