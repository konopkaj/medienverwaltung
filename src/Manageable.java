
public interface Manageable
{
	public void add(Medium m);
	
	public void remove(Medium m);
	
	public void update(Medium m);
	
	public MediaList search(String searchTerm);
	
	public MediaList search(Constants.SEARCH_KEYS key, String value);

}
