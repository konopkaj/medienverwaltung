import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "medium")
public class Medium
{
	private int invNr;
	private String author;
	private String title;
	private String year;
	private String publisher;
	private String keys;
	private Constants.MEDIUM_TYPE type;
	
	/**
	 * @return the invNr
	 */
	public int getInvNr()
	{
		return invNr;
	}
	/**
	 * @return the author
	 */
	public String getAuthor()
	{
		return author;
	}
	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}
	/**
	 * @return the year
	 */
	public String getYear()
	{
		return year;
	}
	/**
	 * @return the publisher
	 */
	public String getPublisher()
	{
		return publisher;
	}
	/**
	 * @return the keys
	 */
	public String getKeys()
	{
		return keys;
	}
	/**
	 * @return the type
	 */
	public Constants.MEDIUM_TYPE getType()
	{
		return type;
	}

	/**
	 * @param invNr the invNr to set
	 */
	public void setInvNr(int invNr)
	{
		this.invNr = invNr;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author)
	{
		this.author = author;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(String year)
	{
		this.year = year;
	}
	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher)
	{
		this.publisher = publisher;
	}
	/**
	 * @param keys the keys to set
	 */
	public void setKeys(String keys)
	{
		this.keys = keys;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(Constants.MEDIUM_TYPE type)
	{
		this.type = type;
	}
}
