import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="inventory")
public class Inventory implements Serializable, Manageable
{
	MediaList media = new MediaList();
	
	/**
	 * @return the media
	 */
	public MediaList getMedia()
	{
		return media;
	}

	/**
	 * @param media the media to set
	 */
	public void setMedia(MediaList media)
	{
		this.media = media;
	}

	@Override
	public void add(Medium m)
	{
		media.add(m);
	}

	@Override
	public void remove(Medium m)
	{
		for (Medium med : media)
		{
			if(med.getInvNr() == m.getInvNr())
			{
				media.remove(med);
				break;
			}
		}
	}

	@Override
	public void update(Medium m)
	{
		remove(m);
		add(m);
	}

	@Override
	public MediaList search(String searchTerm)
	{
		MediaList searchResult = new MediaList();
		
		for (Medium med : media)
		{
			if(med.getKeys().contains(searchTerm))
			{
				searchResult.add(med);
			}
		}
		
		return searchResult;
	}

	@Override
	public MediaList search(Constants.SEARCH_KEYS key, String value)
	{
		MediaList searchResult = new MediaList();
		
		for (Medium med : media)
		{
			switch (key)
			{
				case author:
					if(med.getAuthor().equalsIgnoreCase(value))
					{
						searchResult.add(med);
					}
					break;
				case title:
					if(med.getTitle().equalsIgnoreCase(value))
					{
						searchResult.add(med);
					}
					break;
				case year:
					if(med.getYear().equalsIgnoreCase(value))
					{
						searchResult.add(med);
					}
					break;
			}
		}		
		return searchResult;
	}


	@Override
	public void load(String path, String filename)
	{
		
		// TODO Auto-generated method stub

	}

	@Override
	public void save(String path, String filename)
	{
		try
		{
			JAXBContext context = JAXBContext.newInstance(Inventory.class);
		    Marshaller m = context.createMarshaller();
		    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// TODO Auto-generated method stub
		    
		    m.marshal(this, System.out);
		}
		catch (JAXBException ex)
		{
			ex.printStackTrace();
		}
	}
}
