import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "usbstick")
public class UsbStick extends Digital
{
	private Constants.USB_VERSION usbVersion;

	/**
	 * @return the version
	 */
	public Constants.USB_VERSION getUsbVersion()
	{
		return usbVersion;
	}
	
	/**
	 * @param version the version to set
	 */
	public void setUsbVersion(Constants.USB_VERSION usbVersion)
	{
		this.usbVersion = usbVersion;
	}
}
