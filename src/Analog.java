import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Analog")
public class Analog extends Medium
{
	private int volume;
	private int number;
	private int pages;
	/**
	 * @return the volume
	 */
	public int getVolume()
	{
		return volume;
	}
	/**
	 * @return the number
	 */
	public int getNumber()
	{
		return number;
	}
	/**
	 * @return the pages
	 */
	public int getPages()
	{
		return pages;
	}
	/**
	 * @param volume the volume to set
	 */
	public void setVolume(int volume)
	{
		this.volume = volume;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(int number)
	{
		this.number = number;
	}
	/**
	 * @param pages the pages to set
	 */
	public void setPages(int pages)
	{
		this.pages = pages;
	}
}
