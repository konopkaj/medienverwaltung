import javax.xml.bind.*;

public class Treiber
{

	public static void main(String[] args) throws JAXBException
	{
		Inventory inv = new Inventory();
		
		// TODO Auto-generated method stub
		Book buch = new Book();
		buch.setAuthor("Test");
		buch.setPages(10);
		buch.setISBN("ASFG");
		inv.add(buch);
		
		CD cd = new CD();
		cd.setAuthor("ICH");
		cd.setCopyProtected(true);
		inv.add(cd);
		
		HDD hdd = new HDD();
		hdd.setSize(Constants.HDD_SIZE.ZOLL_2_5);
		hdd.setAuthor("DU");
		inv.add(hdd);

		inv.save("/tmp/", "inv.xml");
	}

}
