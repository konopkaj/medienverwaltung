
public interface Constants
{
	public static enum USB_VERSION {USB1, USB2, USB3};
	
	public static enum MEDIUM_TYPE {
		BOOK,
		JOURNAL,
		DVD,
		CD,
		BLUERAY,
		USBSTICK,
		HARDDISK
	}
	
	public static enum HDD_SIZE { ZOLL_2_5, ZOLL_3_5};
	
	public static enum SEARCH_KEYS { author, title, year};
}
